# TypeWright_Keybow
Files and supporting documents to use a Pimoroni Keybow to edit texts using the TypeWright interface at 18thConnect.org.

To read the documentation about how to make and program a Typewright Keybow, visit http://di.salemstate.edu/snowka/documentation/Typewright_Keybow_Documentation/docs/build/html/index.html
